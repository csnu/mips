
# -----------------------------------------changing array values within array---------------------------------------

.data
	arr: .word  21, 20, 51, 83, 20, 20
.text
	la $t0, arr
	
	lw $t1, 0($t0)
	
	add $t1, $t1, 5
	
	sw $t1, 0($t0)
	
	lw $t3, 0($t0)
	
	li $v0, 1
	move $a0, $t3 
	syscall
