.data
	arr: .word 3 1 4 1 5 9 2 6 5
	size: .word 9
	space: .byte ' '
.text
	la $a0, arr
	
	move $s0, $a0  # s0 = arr
	
	lw $a0, size
	
	add $s2, $a0, $zero  # s1 = size of array
	
	Loop:
		beq $s1, $s2, Exit # if i == size then Exit
		
		sll $t1, $s1, 2 # t1 = s1 * 2^2
		
		add $t1, $t1, $s0 # t1 += s0 getting address of s0 into t1
		
		lw $t0, 0($t1) # t0 = t1[0]
		
		add $t0, $t0, 1 # increment arr[i]++;
		
		sw $t0, 0($t1) # arr[0] = t0
		
		li $v0, 1
		add $a0, $t0, $zero
		syscall
		
		li $v0, 1
		lb $a0, space
		syscall 
		
		add $s1, $s1, 1 # s1++; i++
		j Loop
	Exit:
		li $v0, 10
		syscall
