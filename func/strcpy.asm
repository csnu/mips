.data
	string: .asciiz "hello"
	s: .asciiz ""
.text
	la $t0, string
	la $t2, s
	strcpy:
		beq $s1, 5, Exit
		
		lb $a0, ($t0)
		
		sb $a0, ($t2)
		
		li $v0, 11
		syscall
		
		add $s1, $s1, 1
		add $t0, $t0, 1
		add $t2, $t2, 1
		j strcpy
	Exit:
		li $v0, 10
		syscall	