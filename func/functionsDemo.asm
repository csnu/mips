.data
	number1: .word 1
	number2: .word 23
.text

	lw $t2, number2
	lw $t1, number1
	main:
		jal subtract
		
		li $v0, 1
		move $a0, $t0
		syscall
		
		j Exit  
	addition:
		add $t0, $t1, $t2
		
		jr $ra
	
	subtract:
		sub $t0, $t1, $t2
		
		jr $ra
				
	Exit:
		li $v0, 10
		syscall