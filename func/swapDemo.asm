.data
	arr: .word 1 2
.text

	main: 
		la $a0, arr
		move $s6, $a0
		
		jal swap
		
		j loop
	swap:
		sll $t1, $s3, 2
		add $t1, $zero, $s6
		
		lw $t0, 0($t1)
		lw $t2, 4($t1)
		
		sw $t2, 0($t1)
		sw $t0, 4($t1)
		
		jr $ra
	loop:
		beq $t3, 2, Exit
		sll $t4, $t3, 2
		add $t4, $t4, $s6
		
		lw $t5, 0($t4)
		
		li $v0, 1
		move $a0, $t5
		syscall
		
		addi $t3, $t3, 1
		
		j loop
	Exit:
		li $v0, 10
		syscall
