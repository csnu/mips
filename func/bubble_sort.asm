.data
	arr: 2 4 3 6 1
.text
	la $s0, arr
	# i j temp $t0 $t1 $t2
	# s3 s4 arr[i] arr [i+1]
	# s1 s2 are i i + 1
	bubble_sort:
		loop1:
			beq $t0, 5, Exit
			sll $s1, $t0, 2 # index of arr
			add $s1, $s1, $s0 # address of s0[i]
			
			lw $s3, 0($s1)
			
			j loop2
			
			add $t0, $t0, 1
		loop2:
			beq $t1, 5, continue
			
			sll $s2, $t1, 2
			add $s2, $s2, $s0
			
			lw $s4, 0($s2)
			
			bgt $s1, $s3, swap
			
			add $t1, $t1, 1
			
			j loop2
			
		swap:
			move $t7, $s3
			move $s3, $s4
			move $s4, $t7
		continue:
			jr $ra
		
		
		Exit:			