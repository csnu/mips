.data
.text
	#a, b, c are $t0, $t1, $t2
	# n is $s0
	
	main: 
		add $s0, $zero, 5
		add $t1, $zero, 1
		add $t2, $t1, $t0
		j fib 
	fib:
		bgt $t0, $s0, Exit
		
		li $v0, 1
		move $a0, $t0
		syscall
		
		move $t0, $t1
		move $t1, $t2
		add $t2, $t1, $t0
		
		j fib
		
	Exit:
		li $v0, 10
		syscall 
