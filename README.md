# CSCI231 - Computer Systems and Organization

## About
Class from Computer Science, Nazarbayev University. Fall 2019. In this course we studied MIPS Assembly and learned how to code in low-level languages that are closer to machine code.
