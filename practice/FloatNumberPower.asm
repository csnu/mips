.data
	num: .float 0.5
.text

	main:
		lwc1 $f6, num
		
		li $t2, 3
		mfc1 $t1, $f6
		
		li $t0, 1
		
		#li $v0, 2
		#move $a0, $t0
		#syscall
		
		j exit
	power:
		beq $t2, 0, exitPower
		
		mtc1 $t1, $f2
		mtc1 $t0, $f4
		
		mul.s $f12, $f2, $f4
		
		mfc1 $t0, $f12
		
		li $v0, 1
		move $a0, $t0
		syscall
		subi $t2, $t2, 1
		
		j power
	exitPower:
		jr $ra
		
	exit: