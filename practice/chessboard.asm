# a program fragment which draws a chessboard
.data
	DISPLAY: .space 0x00100 # 8*8*4, we need to reserve this space at the beginning of .data segment
	DISPLAYWIDTH: .word 8
	DISPLAYHEIGHT: .word 8
	red: .word 0xff0000
	green: .word 0x00ff00
.text
j main

set_pixel_color:
# Assume a display of width DISPLAYWIDTH and height DISPLAYHEIGHT
# Pixels are numbered from 0,0 at the top left
# a0: x-coordinate
# a1: y-coordinate
# a2: color
# address of pixel = DISPLAY + (y*DISPLAYWIDTH + x)*4
#			y rows down and x pixels across
# write color (a2) at arrayposition

	lw $t0, DISPLAYWIDTH
	mul $t0, $t0, $a1 	# y*DISPLAYWIDTH
	add $t0,$t0, $a0 	# +x
	sll $t0, $t0, 2 	# *4
	la $t1, DISPLAY 	# get address of display: DISPLAY
	add $t1, $t1, $t0	# add the calculated address of the pixel
	sw $a2, ($t1) 		# write color to that pixel
	jr $ra 			# return
	

main:	
	li $s0, 0
	li $s1, 0
	li $s4, 2
	li $s5, 8
	row:
		li $s0, 0 
		col:
			add $s3, $s0, $s1
			div $s3, $s4
			
			mfhi $s3
			move $a0, $s0
			move $a1, $s1
			
			beq $s3, $zero, even			
			lw $a2, green
			jal set_pixel_color
			addi $s0, $s0, 1
			beq $s0, $s5, exitrow
			j col
			even:
				lw $a2, red
				jal set_pixel_color
				addi $s0, $s0, 1
			    beq $s0, $s5, exitrow
			    j col
			exitrow:
				addi $s1, $s1, 1
				beq $s1, $s5, exitcol
				j row
				    
	exitcol:
		li $v0, 10
		syscall					
	
