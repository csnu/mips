.data
	num: .word 5
	answer: .word 0
	newline: .asciiz "\n"
	space: .asciiz "   "
	angle: .float 0.5 	# 30 degrees
.text
	# t1 = base, X		constant
	# t2 = exponent		t2 = n, n-1, n-2,...
	# t0 = base ^ exponent 	t0 = t2!
	
	# $t4 = sum
	li $t4, 1
	li $t2, 2
	
	
	for:
		move $t5, $t2		# t5 = t2 = Exponent	constant
		addi $t1, $zero, 5
		
		addi $t0, $zero, 1
	
		jal power
	
		li $v0, 1
		move $a0, $t0		# t0 = x^ 2, 4, 8, ...
		syscall
		
		move $s0, $t0		# s0 = t0
		
		move $t2, $t5
		li $t0, 1	
	
		jal factorial

		li $v0, 4
		la $a0, newline
		syscall
			
		li $v0, 1
		move $a0, $t0		# t0 = 2!, 4!, 6!, ...
		syscall
		
		mtc1 $s0, $f1
		mtc1 $t0, $f2
		
		div.s $f12, $f1, $f2
		
		mfc1 $s1, $f12
		
		li $v0, 4
		la $a0, newline
		syscall
		
		li $v0, 2
		move $a0, $s1
		syscall
		
		
		move $t2, $t5
		
		addi $t2, $t2, 2
		
		li $v0, 4
		la $a0, newline
		syscall
		
		beq $t5, 6, exit
		
		j for
	
	power:
		beq $t2, 0, exitPower
		
		
		mul $t0, $t0, $t1
		
		subi $t2, $t2, 1
		
		j power
	exitPower:
		jr $ra
			
	factorial:
		beq $t2, 0, exitFactorial
		
		mul $t0, $t0, $t2
		
		subi $t2, $t2, 1
		
		j factorial
	
	exitFactorial:
		jr $ra
		
	exit:
