.data
	fibarray: .word 0, 1, 1, 2, 3, 5, 8
	space: .asciiz " "	
.text
	
	la $s0, fibarray
	
	add $t0, $0, $0
	addi $t1, $0, 4
	addi $t2, $0, 8
	addi $t3, $0, 12
	addi $t4, $0, 16
	addi $t5, $0, 20
	addi $t6, $0, 24	
	
	li $v0, 1
	lw $a0, fibarray($t0)
	syscall
	
	#space
	li $v0, 4
	la $a0, space
	syscall
	#end space
	
	li $v0, 1
	lw $a0, fibarray($t1)
	syscall
	
	#space
	li $v0, 4
	la $a0, space
	syscall
	#end space
	
	li $v0, 1
	lw $a0, fibarray($t2)
	syscall
	
	#space
	li $v0, 4
	la $a0, space
	syscall
	#end space
	 
	li $v0, 1
	lw $a0, fibarray($t3)
	syscall
	
	#space
	li $v0, 4
	la $a0, space
	syscall
	#end space
	
	li $v0, 1
	lw $a0, fibarray($t4)
	syscall
	
	#space
	li $v0, 4
	la $a0, space
	syscall
	#end space
	
	li $v0, 1
	lw $a0, fibarray($t5)
	syscall
	
	#space
	li $v0, 4
	la $a0, space
	syscall
	#end space
	
	li $v0, 1
	lw $a0, fibarray($t6)
	syscall
	
	#space
	li $v0, 4
	la $a0, space
	syscall
	#end space
	
	sw $t0, fibarray($t0)
	sw $t1, fibarray($t1)
	sw $t2, fibarray($t2)
	sw $t3, fibarray($t3)
	sw $t4, fibarray($t4)
	sw $t5, fibarray($t5)
	sw $t6, fibarray($t6)
		
