.data
	DISPLAY: .space 16384 # 8*8*4, we need to reserve this space at the beginning of .data segment
	DISPLAYWIDTH: .word 64
	DISPLAYHEIGHT: .word 64
	RED: .word 0xff0000
	newline: .asciiz "\n"
.text

	j main
set_pixel_color:
# Assume a display of width DISPLAYWIDTH and height DISPLAYHEIGHT
# Pixels are numbered from 0,0 at the top left
# a0: x-coordinate
# a1: y-coordinate
# a2: color
# address of pixel = DISPLAY + (y*DISPLAYWIDTH + x)*4
#			y rows down and x pixels across
# write color (a2) at arrayposition

	lw $t0, DISPLAYWIDTH
	mul $t0, $t0, $a1 	# y*DISPLAYWIDTH
	add $t0,$t0, $a0 	# +x
	sll $t0, $t0, 2 	# *4
	la $t1, DISPLAY 	# get address of display: DISPLAY
	add $t1, $t1, $t0	# add the calculated address of the pixel
	sw $a2, ($t1) 		# write color to that pixel
	jr $ra 			# return
	


main:

	li $s1, -30 	# s1 = r
	li $s0, 0	# s0 = x
	
	mul $s2, $s1, $s1 # s2 = r^2
	
	for: 
		
		mul $s3, $s0, $s0	# s3 = ( x ) ^ 2
		
		
		sub $s3, $s2, $s3	# y ^2 = r^2 - x^2
		 
		mtc1 $s3, $f1
		cvt.s.w $f1, $f1
		sqrt.s $f0, $f1
		round.w.s $f0, $f0
		
		addi $t7, $s0, 20
		mfc1 $s3, $f0		# s3 = y
		addi $s3, $s3, 20	# s3 = y - 20

		move $a0, $t7
		move $a1, $s3
		
		lw $a2, RED
		jal set_pixel_color
		
		addi $s0, $s0, 1
		
		beq $s0, $s1, exit
		
		j for
	
		
	exit:
		li $v0, 10
		syscall
