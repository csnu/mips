.data
	msg1: .asciiz "Enter a base: "
	msg2: .asciiz "Enter a exponent: "
	msg3: .asciiz "\nPower is "
	base: .word 0
	answer: .word 0
	exponent: .word 0
.text
	main:
		
		li $v0, 4
		la $a0, msg1		# printing msg1
		syscall
		
		li $v0, 5
		syscall
		
		sw $v0, base		# giving the input to base
			
		
		lw $t1, base		# t1 = base
		
		
		li $v0, 4
		la $a0, msg2		# printing msg2
		syscall
		
		li $v0, 5
		syscall
		
		sw $v0, exponent	# setting exponent to the input 
		 
		lw $a1, exponent
		jal pow
		sw $v0, answer
		
		li $v0, 4
		la $a0, msg3		# printing msg3
		syscall
		
		li $v0, 1
		lw $a0, answer		# printing answer
		syscall
		
		li $v0, 10		# exit
		syscall
		
	
	pow:
		subu $sp, $sp, 4
		sw $ra, ($sp)
		
		# Base Case
		li $v0, 1
		beq $a1, 0, done
		
		#pow( number - 1)
		sub $a1, $a1, 1
		jal pow
		
		mul $v0, $t1, $v0 
		
	 	done:
			lw $ra, ($sp)
			addu $sp, $sp, 4
			jr $ra
