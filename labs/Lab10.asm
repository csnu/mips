.data
array: .space 1024

.text
#Number of blocks: 	2
#Cache block size: 	16
#YOUR METRIC SCORE:	8.71 * 2048 = 17838.08
#The reasons for my optimization
#In Assembly code:
#In the configurations of cache parameters:

main:
	li $t5, 1024	# t5 = size of array
	
	la $t4, array	# t4 = array
	
	for:
		beq $t3, $t5, exit	# t3 = i, iterator
		
		sll $t1, $t3, 2
		
		add $t1, $t1, $t4	# t1 = array[i]
		lb $t0, 0($t1)		# t0 = array[i]
		
		addi $t0, $t0, 1	# array[i] ++ 
		
		sb $t0, 0($t1)		# store back the incremented value
		
		addi $t3, $t3, 1
		j for
	exit:
		
