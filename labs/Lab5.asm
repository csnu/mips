.data
	arr: .asciiz "Hello"
	size: .word 5
.text
	# s0, s1 are i, j
	# s2 = size
	# t0 address of arr[j] 
	# t1 address of arr[j+1]
	# t2 = arr[j]
	# t3 = arr[j+1]
	# t4 = temp
	lw $s2, size
	addi $s2, $s2, -1
	
	lw $s3, size
	sub $s3, $s3, $s2
	addi $s3, $s3, -1
	
	la $a0, arr
	
	lw $s5, size
sort:
	loop1:
		beq $s0, $s2, print
				
		j loop2
		
		
	loop2:
		beq $s1, $s3, exit2
		sll $t0, $s0, 2
		add $t0, $t0, $a0
		
		lb $t2, 0($t0)	# t2 = arr[j]
		
		lb $t3, 4($t0)  # t3 = arr[j+1]
		
		addi $s1, $s1, 1
		
		bgt $t2, $t3, swap
		
		jal loop2		
	swap:
		#move $t4, $t2 # temp = arr[j]
		sw $t3, 0($t0)
		sw $t2, 4($t0)
		
		jr $ra
	
	exit2:
		addi $s0, $s0, 1
		
		lw $s3, size
		sub $s3, $s3, $s1
		addi $s3, $s3, -1
		
		j loop1
	print:
		li $v0, 4
		la $a0, arr
		syscall
		loop:
			beq $t5, $s5, exit1
			sll $t6, $t5, 2
			add $t6, $t6, $a0
			
			la $t7, 0($t6)
			
			li $v0, 1
			move $a0, $t7
			syscall
			
			addi $t5, $t5, 1
			
			j loop
		
exit1: 
		li $v0, 10
		syscall