.data
	arr: .word 21 20 51 83 20 20 21 20 33	#array
	space: .asciiz " "
	msg1: .asciiz "x = "
	msg2: .asciiz "y = "
	nl: "\n"
	size: .word 9			#size of the array
	x: .word 20			#replace x by y
	y: .word 5						

.text
	la $s6, arr 			# s6 =  arr
	
	lw $a0, size			
	add $s5, $a0, $zero 		# s5 = size
	
	lw $a0, x			
	add $t6, $a0, $zero		# t6 = x
	
	li $v0, 4
	la $a0, msg1			# printing " x = " 
	syscall 
	
	li $v0, 1
	add $a0, $zero, $t6 		#printing x itself
	syscall
	
	li $v0, 4	
	la $a0, nl			#printing new line
	syscall
		
	lw $a0, y
	add $t5, $zero, $a0		# t5 = y
	
	li $v0, 4
	la $a0, msg2			# printing " x = " 
	syscall 
	
	li $v0, 1
	lw $a0, y
	syscall
	
	main: 
		jal print
		
		jal replace
		
		jal print
		
		jal exitfinal
	#==================== replace function x by y =====================
	replace:
		loop:
			beq $s3, $s5, exit
			sll $t1, $s3, 2
		
			add $t1, $t1, $s6
			lw $t0, 0($t1)
			addi $s3, $s3, 1
			bne $t0, $t6, loop
		
			sw $t5, 0($t1)
		
			j loop
	exit:
		jr $ra
	
	found: 
		sw $t5, 0($t1)
		
		jr $ra
		
	print:
		li $v0, 4
		la $a0, nl
		syscall
		
		add $s2, $zero, $zero
		while:
			beq $s2, $s5, exit
			sll $t1, $s2, 2
		
			add $t1, $t1, $s6
			lw $t0, 0($t1)
		
			addi $s2, $s2, 1
		
			li $v0, 1
			add $a0, $t0, $zero
			syscall
		
			li $v0, 4
			la $a0, space
			syscall
		
			j while
	
	exitfinal:
		li $v0, 10
		syscall