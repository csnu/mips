.data
	arr: .word 1 1 1 1 6 1 1 2 6 	#array
	space: .asciiz " "
	msg1: .asciiz "x = "
	nl: "\n"
	size: .word 9			#size of the array
	x: .word 6			#replace x by y
	y: .word 8						

.text
	la $s6, arr 			# s6 =  arr
	
	lw $a0, size			
	add $s5, $a0, $zero 		# s5 = size
	
	lw $a0, x			
	add $t6, $a0, $zero		# t6 = x
	
	li $v0, 4
	la $a0, msg1			# printing " x = " 
	syscall 
	
	li $v0, 1
	add $a0, $zero, $t6 		#printing x itself
	syscall
	
	li $v0, 4	
	la $a0, nl			#printing new line
	syscall
		
	lw $a0, y
	add $t5, $zero, $a0		# t5 = y
	
	
	main: 
		jal replace
		
	#==================== raplace function x by y =====================
	replace:
		loop:
		beq $s3, $s5, exit
		sll $t1, $s3, 2
		
		add $t1, $t1, $s6
		lw $t0, 0($t1)
		
		addi $s3, $s3, 1
		
		beq $t0, $t6, found
		
		li $v0, 1
		add $a0, $t0, $zero
		syscall
		
		li $v0, 4
		la $a0, space
		syscall
		
		j loop
	exit:
		li $v0, 10
		syscall
	
	found:
		li $v0, 1
		lw $a0, y
		syscall
		
		li $v0, 4
		la $a0, space
		syscall
		
		jr $ra
