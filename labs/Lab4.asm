.data 
	size: .word 5
	nl: .asciiz "\n"
	space: .asciiz " "
	string: .space 25
	reversedString: .space 25
	message1: .asciiz "Enter the string size: "
	message2: .asciiz "Enter the string: "
.text
	main:
		li $v0, 4
		la $a0, message1	#printing message1
		syscall
		
		li $v0, 5		#scanf("%d")
		syscall
		
		move $s0, $v0		# s0 = size
		
		sub $s2, $s0, 1 	#last index of string
		
		li $v0, 4
		la $a0, message2	#printing message2
		syscall
		
		la $a0, string		
		li $a1, 25
		li $v0, 8		#scanf("%s")
		syscall
		
		move $s6, $a0
		add $s6, $s6, $s2	# now s6 points to last index in string
		
		la $t0, reversedString
	reverse:
		beq $s1, $s0, Exit	# s1 = i, s0 = size
		
		lb $a0, ($s6)		#a0 = s[end]
		
				
		sb $a0, ($t0)		#s[begin] = a0
						
		add $s1, $s1, 1 	# s1 = i; i++
		
		add $t0, $t0, 1		# begin++;
		sub $s6, $s6, 1		# end--;
		
		j reverse
	Exit: 
		li $v0, 4
		la $a0, reversedString	#printing reversed string 
		syscall
	
		li $v0, 10
		syscall
