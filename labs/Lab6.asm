.data
	num: .word 2147483647
	msg1: .asciiz "Enter the first number: "
	msg2: .asciiz "Enter the second number: "
	msg3: .asciiz "Sum is "
.text
main:
	Loop:
		li $v0, 4
		la $a0, msg1		#print msg1
		syscall
		
		li $v0, 5		#scanf("%d")
		syscall
		
		move $t0, $v0		# t0 = number1
		
		li $v0, 4
		la $a0, msg2		#print msg2
		syscall
		
		li $v0, 5		#scanf("%d")
		syscall
		
		move $t1, $v0		# t1 = number2
		
		add $t2, $t1, $t0	# t2 = number1 + number2
		
		li $v0, 4
		la $a0, msg3		#print msg3
		syscall
		
		li $v0, 1
		move $a0, $t2		# print sum
		syscall	

		
.ktext 0x80000180
	lw $t3, address
	mtc0 $t3, $14		# move address in $t3 to $14	
	
	li $v0, 4
	la $a0, msg
	syscall
	
	
	eret 
.kdata
	msg: .asciiz "Try entering smaller numbers!\n\n"
	address: .word 0x00400000
