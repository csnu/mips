.data
	notEqualString: .asciiz "Not Equal hence no loop :("
	equalString: .asciiz "Equal here's the loop: "
	space: .asciiz " "
	nl: .asciiz "\n"
	lessthanString: "less than the other"
	greaterThanString: .asciiz "greather than the other"
.text
	add $t1, $zero, 255
	
	nor $t0, $t1, $zero
	
	#li $v0, 1
	#move $a0, $t0
	#syscall
	
	j Condition
	add $t2, $zero, 6
	add $t3, $zero, 6
	
	beq $t2, $t3, Equal
	
	li $v0, 4
	la $a0, notEqualString
	syscall
	
	j Exit
	
	Equal:
		li $v0, 4
		la $a0, equalString
		syscall
		
		li $v0, 4
		la $a0, nl
		syscall
		
		j Loop
	
	Loop:
		beq $t4, $t3, Exit
		
		li $v0, 1
		move $a0, $t4
		syscall 
		
		li $v0, 4
		la $a0, space
		syscall
		
		add $t4, $t4, 1
		
		j Loop
	Condition:
		add $t5, $zero, 8
		add $t6, $zero, 7
		
		#slt $s0, $t5, $t6
		bgt $t5, $t6, LessThan
		#bne $s0, $zero, LessThan 
		
		li $v0, 4
		la $a0, greaterThanString
		syscall
		
		j Exit
	LessThan:
		li $v0, 4
		la $a0, lessthanString
		syscall
	Exit:
		li $v0, 10
		syscall