.data
	number1: .word 23
	number2: .word 9
.text
	lw $s0, number2 # s0 = number1
	lw $s1, number1 # s1 = number2
	
	sub $t0, $s0, $s1 # t0 = s0 - s1
	
	li $v0, 1
	move $a0, $t0
	syscall
