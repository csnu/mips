.data
	message1: .asciiz "Different"
	message2: .asciiz "Equal"
.text
	main: 
		addi $t0, $zero, 5
		addi $t1, $zero, 5
	
		bne $t0, $t1, numbersDifferent
		
		#beq $t0, $t1, numbersEqual
		#to end main
		li $v0, 10
		syscall
		
	numbersDifferent:
		li $v0, 4
		la $a0, message1
		syscall
	
	numbersEqual:
		li $v0, 4
		la $a0, message2
		syscall