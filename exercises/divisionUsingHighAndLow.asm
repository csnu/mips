.data
	quotient: .ascii "Quotient: "
	remainder: .ascii "\nRemainder: "
	newline: .ascii "\n"
.text
	addi $t0, $zero, 30
	addi $t1, $zero, 7
	
	div $t0, $t1
	
	mflo $s0 # quotient
	mfhi $s1 # remainder
	
	#print the word "quotient"
	li $v0, 4
	la $a0, quotient
	syscall
	
	#print quotient
	li $v0, 1
	add $a0, $zero, $s0
	syscall
	
	#print the word "remainder"
	li $v0, 4
	la $a0, remainder
	syscall
	
	#print remainder
	li $v0, 1
	add $a0, $zero, $s1
	syscall