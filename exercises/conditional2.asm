.data
.text
	addi $s1, $zero, 7
	addi $s2, $zero, 9
	
	addi $s3, $zero, 4
	addi $s4, $zero, 5
	
	
	bne $s3, $s4, notEqual
	add $s0, $s1, $s2
	j Exit
	
	notEqual: 
		sub $s0, $s1, $s2
	Exit: 
		li $v0, 10
		syscall
	
	li $v0, 1
	add $a0, $s0, $zero
	syscall
